﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using log4net;
using DAO;

namespace Test
{
    class PostgreSqlTest
    {
        #region filed+ctor
        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string _m_config_string;
        public PostgreSqlTest()
        {

        }
        public PostgreSqlTest(string config_string)
        {
            _m_config_string = config_string;
        }
        #endregion
        #region Test Db Connection
        public static bool TestDbConnection(string conn)
        {
            try
            {
                using (var my_conn = new NpgsqlConnection(conn))
                {
                    my_conn.Open();
                    return true;
                }
            }
            catch (Exception ex)
            {
                my_logger.Error("Cannot connect to db" + ex.Message);
                return false;
            }
        }
        #endregion
        #region Stored Procedure
        private List<Dictionary<string, object>> Run_sp(string conn_string, string sp_name,
            NpgsqlParameter[] parameters)
        {
            List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();

            try
            {
                using (var conn = new NpgsqlConnection(conn_string))
                {
                    conn.Open();

                    NpgsqlCommand command = new NpgsqlCommand(sp_name, conn);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.AddRange(parameters);

                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Dictionary<string, object> one_row = new Dictionary<string, object>();
                        foreach (var item in reader.GetColumnSchema())
                        {
                            object column_value = reader[item.ColumnName];
                            one_row.Add(item.ColumnName, column_value);
                        }
                        items.Add(one_row);
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Function {sp_name} failed. parameters: {string.Join(",", parameters.Select(_ => _.ParameterName + " : " + _.Value))} exception: { ex.Message}");
                Console.WriteLine($"Function {sp_name} failed. parameters: {string.Join(",", parameters.Select(_ => _.ParameterName + " : " + _.Value))}");
            }
            return items;
        }

        public List<Dictionary<string, object>> SP_AllWorkersFromSite(string siteName)
        {
            var allWorkersFromSite = Run_sp(_m_config_string, "sp_AllWorkersFromSite", new NpgsqlParameter[]
                {
                    new NpgsqlParameter("_sitename", siteName)
                });
            return allWorkersFromSite;
        }
        #endregion
        #region Execute Non Query
        private int ExecuteNonQuery(string query)
        {
            int result = 0;
            try
            {
                using (NpgsqlCommand cmd = new NpgsqlCommand())
                {
                    using (cmd.Connection = new NpgsqlConnection(_m_config_string))
                    {
                        cmd.Connection.Open();

                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = query;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to make changes in data base. Error : {ex}");
            }
            return result;
        }
        #endregion
        #region Movies methods
        private List<Movie> GetAllMovies(string query)
        {
            List<Movie> movies = new List<Movie>();
            try
            {
                using (var conn = new NpgsqlConnection(_m_config_string))
                {
                    conn.Open();

                    NpgsqlCommand command = new NpgsqlCommand(query, conn);
                    command.CommandType = System.Data.CommandType.Text;

                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        movies.Add(new Movie
                        {
                            Id = (long)reader["id"],
                            Name = reader["name"].ToString(),
                            Release_Date = Convert.ToDateTime(reader["release_date"]),
                            Genre_Id = (long)reader["genre_id"]
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"cant get movies: {ex.Message}");
                my_logger.Error($"cant get movies: {ex.Message}");
            }
            return movies;
        }
        public List<Movie> GetAllMovies()
        {
            return GetAllMovies("SELECT * FROM movies");
        }
        public List<Movie> GetAlMoviesById(int id)
        {
            return GetAllMovies($"SELECT * FROM movies Where id = {id}");
        }
        public void AddMovie(Movie movie)
        {
            string day = movie.Release_Date.Day.ToString();
            string month = movie.Release_Date.Month.ToString();
            if (movie.Release_Date.Month < 10)
                month = $"0{movie.Release_Date.Month}";
            if (movie.Release_Date.Day < 10)
                day = $"0{movie.Release_Date.Day}";
            string date = $"{movie.Release_Date.Year}-{month}-{day}";
            ExecuteNonQuery($"INSERT INTO movies (name, release_date, genre_id) VALUES ('{movie.Name}','{date}',{movie.Genre_Id})");
        }
        public void UpdateMovie(Movie movie, int id)
        {
            string day = movie.Release_Date.Day.ToString();
            string month = movie.Release_Date.Month.ToString();
            if (movie.Release_Date.Month < 10)
                month = $"0{movie.Release_Date.Month}";
            if (movie.Release_Date.Day < 10)
                day = $"0{movie.Release_Date.Day}";
            string date = $"{movie.Release_Date.Year}-{month}-{day}";
            ExecuteNonQuery($"UPDATE movies SET name = '{movie.Name}', release_date = '{date}', genre_id = {movie.Genre_Id} WHERE id = {id}");
        }
        public void DeleteMovie(int id)
        {
            ExecuteNonQuery($"DELETE FROM movies WHERE id = {id}");
        }
        #endregion
        #region Genres methods
        private List<Genre> GetAllGenres(string query)
        {
            List<Genre> genres = new List<Genre>();
            try
            {
                using (var conn = new NpgsqlConnection(_m_config_string))
                {
                    conn.Open();

                    NpgsqlCommand command = new NpgsqlCommand(query, conn);
                    command.CommandType = System.Data.CommandType.Text;

                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        genres.Add(new Genre
                        {
                            Id = (long)reader["id"],
                            Name = reader["name"].ToString()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"cant get movies: {ex.Message}");
                my_logger.Error($"cant get movies: {ex.Message}");
            }
            return genres;
        }
        public List<Genre> GetAllGenres()
        {
            return GetAllGenres("SELECT * FROM genres");
        }
        public List<Genre> GetAllGenresById(int id)
        {
            return GetAllGenres($"Select * from genres where id = {id}");
        }
        public void AddGenre(Genre genre)
        {
            ExecuteNonQuery($"INSERT INTO genres (name) values ('{genre.Name}')");
        }
        public void UpdateGenre(Genre genre, int id)
        {
            ExecuteNonQuery($"UPDATE genres SET name = '{genre.Name}' WHERE id = {5}");
        }
        public void DeleteGenre(int id)
        {
            ExecuteNonQuery($"Delete FROM genres where id = {id}");
        }
        #endregion
        #region Actors methods
        private List<Actor> GetAllActors(string query)
        {
            List<Actor> actors = new List<Actor>();
            try
            {
                using (var conn = new NpgsqlConnection(_m_config_string))
                {
                    conn.Open();

                    NpgsqlCommand command = new NpgsqlCommand(query, conn);
                    command.CommandType = System.Data.CommandType.Text;

                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        actors.Add(new Actor
                        {
                            Id = (long)reader["id"],
                            Name = reader["name"].ToString(),
                            Birth_Date = Convert.ToDateTime(reader["birth_date"]),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"cant get movies: {ex.Message}");
                my_logger.Error($"cant get movies: {ex.Message}");
            }
            return actors;
        }
        public List<Actor> GetAllActors()
        {
            return GetAllActors("SELECT * FROM actors");
        }
        public List<Actor> GetActorById(int id)
        {
            return GetAllActors($"SELECT * FROM actors where id = {id}");
        }
        public void AddActor(Actor actor)
        {
            string day = actor.Birth_Date.Day.ToString();
            string month = actor.Birth_Date.Month.ToString();
            if (actor.Birth_Date.Month < 10)
                month = $"0{actor.Birth_Date.Month}";
            if (actor.Birth_Date.Day < 10)
                day = $"0{actor.Birth_Date.Day}";
            string date = $"{actor.Birth_Date.Year}-{month}-{day}";
            ExecuteNonQuery($"INSERT INTO actors (name, birth_date) VALUES ('{actor.Name}', '{date}')");
        }
        public void UpdateActor(Actor actor, int id)
        {
            string day = actor.Birth_Date.Day.ToString();
            string month = actor.Birth_Date.Month.ToString();
            if (actor.Birth_Date.Month < 10)
                month = $"0{actor.Birth_Date.Month}";
            if (actor.Birth_Date.Day < 10)
                day = $"0{actor.Birth_Date.Day}";
            string date = $"{actor.Birth_Date.Year}-{month}-{day}";
            ExecuteNonQuery($"UPDATE actors SET name = '{actor.Name}', birth_date = '{date}' where id = {id}");
        }
        public void DeleteActor(int id)
        {
            ExecuteNonQuery($"DELETE FROM actors where id = {id}");
        }
        #endregion
        #region Get All Movies When Actor were born Before 1972
        public List<object> GetAllMoviesWhenActorBefore1972()
        {
            List<object> result = new List<object>();
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(_m_config_string))
                {
                    conn.Open();
                    string query = "SELECT movies.id,movies.name,movies.release_date,g.name genre FROM movies join genres g on movies.genre_id = g.id" +
                        " WHERE movies.id in (select movie_id from movies_actors where movies_actors.actor_id IN(select id from actors where birth_date<'1972-01-01'));";
                    using (NpgsqlCommand select_query = new NpgsqlCommand(query, conn))
                    {
                        using (NpgsqlDataReader reader = select_query.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var one_row = new
                                {
                                    Id = (long)reader["Id"],
                                    Name = reader["name"].ToString(),
                                    Release_Date = Convert.ToDateTime(reader["release_date"]),
                                    Genre_Name = reader["genre"].ToString()
                                };
                                result.Add(one_row);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to activate query. Error : {ex}");
            }
            return result;
        }
        #endregion
    }
}
