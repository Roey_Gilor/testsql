﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAO;

namespace Test
{
      class SsmsTest
    {
        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region Ctor+field
        private string m_conn_string;

        public SsmsTest(string conn_string)
        {
            m_conn_string = conn_string;
        }
        #endregion
        #region Get stores method
        private List<Store> GetStores(string query)
        {
            List<Store> stores = new List<Store>();
            try
            {
                using (SqlConnection connection = new SqlConnection(m_conn_string))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.Default);
                    while (reader.Read())
                    {
                        stores.Add(
                            new Store
                            {
                                Id = (long)reader["Id"],
                                Name = reader["Name"].ToString(),
                                Floor = (int)reader["Floor"],
                                Category_Id = (long)reader["Category_Id"]
                            });
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to Get all cars. Error : {ex}");
            }
            return stores;
        }
        #endregion
        #region Execute Non Query
        private int ExecuteNonQuery(string query)
        {
            int result = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(m_conn_string))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.CommandType = CommandType.Text;
                    result = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to change data. Error : {ex}");
            }
            return result;
        }
        #endregion
        #region Stores methods
        public List<Store> GetAllStores()
        {
            return GetStores("SELECT * FROM Stores");
        }
        public List<Store> GetStoreById(int id)
        {
            return GetStores($"SELECT * FROM Stores where Id = {id}");
        }
        public void AddStore(Store store)
        {
            ExecuteNonQuery($"INSERT INTO Stores (Name,Floor,Category_Id) VALUES ('{store.Name}', {store.Floor}, {store.Category_Id})");
        }
        public void UpdateStore(Store store, int id)
        {
            ExecuteNonQuery($"UPDATE Stores SET Name = '{store.Name}', Floor = '{store.Floor}', Category_Id = {store.Category_Id} WHERE Id= {id}");
        }
        public void DeleteStore (int id)
        {
            ExecuteNonQuery($"DELETE FROM Stores WHERE Id = {id}");
        }
        #endregion
        #region Get Categories
        private List<Categories> GetCategories(string query)
        {
            List<Categories> categories = new List<Categories>();
            try
            {
                using (SqlConnection connection = new SqlConnection(m_conn_string))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.Default);
                    while (reader.Read())
                    {
                        categories.Add(
                            new Categories
                            {
                                Id = (long)reader["Id"],
                                Name = reader["Name"].ToString(),
                            });
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to Get all cars. Error : {ex}");
            }
            return categories;
        }
        #endregion
        #region Categories methods
        public List<Categories> GetAllCategories()
        {
            return GetCategories("SELECT * FROM Categories");
        }
        public List<Categories> GetCategoryById(int id)
        {
            return GetCategories($"SELECT * FROM Categories WHERE Id = {id}");
        }
        public void AddCategory(Categories category)
        {
            ExecuteNonQuery($"INSERT INTO Categories (Name) VALUES ('{category.Name}')");
        }
        public void UpdateCategory(Categories category,int id)
        {
            ExecuteNonQuery($"UPDATE Categories SET Name = '{category.Name}' WHERE Id = {id}");
        }
        public void DeleteCategory(int id)
        {
            ExecuteNonQuery($"DELETE FROM Categories WHERE Id = {id}");
        }
        #endregion
        #region Extra methods
        public List<object> GetAllShoopsInCategoryAndFloor(int floor, string catName)
        {
            List<object> result = new List<object>();
            try
            {
                using (SqlConnection connection = new SqlConnection(m_conn_string))
                {
                    string query = string.Format($"SELECT c.Name Category_Name,s.Name Shop_Name,s.Floor FROM Categories c join Stores s on c.Id=s.Category_Id " +
                                    "where s.Floor = {0} and c.Name = '{1}'", floor, catName);
                    connection.Open();
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.Default);
                    while (reader.Read())
                    {
                        var row = new
                        {
                            Category_Name = reader["Category_Name"].ToString(),
                            Shop_Name = reader["Shop_Name"].ToString(),
                            Floor=(int)reader["Floor"]
                        };
                        result.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to Get all cars joined to tests. Error : {ex}");

            }
            return result;
        }
        public List<Categories> GetMostShopsInCategory()
        {
            return GetCategories("SELECT c.Id,c.Name FROM Stores s join Categories c on s.Category_Id=c.Id" +
                " group by c.Id,c.Name having count(s.Category_Id)= (SELECT TOP 1 [num_of_shops]" +
                " from (SELECT count(s.Category_Id)num_of_shops FROM Stores s join Categories c on s.Category_Id=c.Id " +
                "group by s.Category_Id) tbl order by num_of_shops desc);");
        }
        #endregion
    }
}
