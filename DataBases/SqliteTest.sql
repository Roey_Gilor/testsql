BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "Cars" (
	"Id"	INTEGER NOT NULL,
	"Manufacturer"	TEXT NOT NULL,
	"Model"	TEXT NOT NULL,
	"Year"	INTEGER NOT NULL,
	PRIMARY KEY("Id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "Tests" (
	"Id"	INTEGER NOT NULL,
	"Car_Id"	INTEGER NOT NULL,
	"IsPassed"	bool NOT NULL,
	"Date"	datetime NOT NULL,
	PRIMARY KEY("Id"),
	FOREIGN KEY("Car_Id") REFERENCES "Cars"("Id")
);
INSERT INTO "Cars" VALUES (1,'china','honda',2012);
INSERT INTO "Cars" VALUES (2,'Germany','Mitsubishi',2019);
INSERT INTO "Cars" VALUES (3,'USA','Yondai',2020);
INSERT INTO "Cars" VALUES (4,'Italy','Reno',2016);
INSERT INTO "Cars" VALUES (5,'Franch','ferary',2011);
INSERT INTO "Tests" VALUES (1,1,1,'2020-02-15');
INSERT INTO "Tests" VALUES (2,2,0,'2020-03-16');
INSERT INTO "Tests" VALUES (3,3,1,'2020-04-17');
INSERT INTO "Tests" VALUES (4,4,0,'2020-05-18');
INSERT INTO "Tests" VALUES (5,5,1,'2020-06-19');
INSERT INTO "Tests" VALUES (6,1,0,'2020-07-20');
INSERT INTO "Tests" VALUES (7,2,1,'2020-08-21');
INSERT INTO "Tests" VALUES (8,3,0,'2020-09-22');
INSERT INTO "Tests" VALUES (9,4,1,'2020-10-23');
COMMIT;
