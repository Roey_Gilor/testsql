USE [Test]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 09/01/2021 22:37:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Stores]    Script Date: 09/01/2021 22:37:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stores](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [text] NOT NULL,
	[Floor] [int] NOT NULL,
	[Category_Id] [bigint] NOT NULL,
 CONSTRAINT [PK_Stores] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Id], [Name]) VALUES (1, N'Electronics')
INSERT [dbo].[Categories] ([Id], [Name]) VALUES (2, N'Books')
INSERT [dbo].[Categories] ([Id], [Name]) VALUES (3, N'Clothes')
INSERT [dbo].[Categories] ([Id], [Name]) VALUES (4, N'Garden')
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[Stores] ON 

INSERT [dbo].[Stores] ([Id], [Name], [Floor], [Category_Id]) VALUES (1, N'Computers', 1, 1)
INSERT [dbo].[Stores] ([Id], [Name], [Floor], [Category_Id]) VALUES (2, N'Iphones', 2, 1)
INSERT [dbo].[Stores] ([Id], [Name], [Floor], [Category_Id]) VALUES (3, N'Cooking books', 3, 2)
INSERT [dbo].[Stores] ([Id], [Name], [Floor], [Category_Id]) VALUES (4, N'Horror books', 1, 2)
INSERT [dbo].[Stores] ([Id], [Name], [Floor], [Category_Id]) VALUES (5, N'Jeans', 2, 3)
INSERT [dbo].[Stores] ([Id], [Name], [Floor], [Category_Id]) VALUES (6, N'Shoes', 3, 3)
INSERT [dbo].[Stores] ([Id], [Name], [Floor], [Category_Id]) VALUES (7, N'Garden tables', 1, 4)
INSERT [dbo].[Stores] ([Id], [Name], [Floor], [Category_Id]) VALUES (8, N'Garden lamps', 2, 4)
INSERT [dbo].[Stores] ([Id], [Name], [Floor], [Category_Id]) VALUES (10, N'Cakes book', 2, 2)
SET IDENTITY_INSERT [dbo].[Stores] OFF
GO
ALTER TABLE [dbo].[Stores]  WITH CHECK ADD  CONSTRAINT [FK_Stores_Categories] FOREIGN KEY([Category_Id])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Stores] CHECK CONSTRAINT [FK_Stores_Categories]
GO
