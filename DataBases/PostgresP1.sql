--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: sp_add_bunos_salary(); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.sp_add_bunos_salary()
    LANGUAGE plpgsql
    AS $$
    begin
        for i in 1.. (SELECT max(id) from "Workers")
        loop
            update "Workers" set salary=salary+500 where id=i;
            end loop;
    end
    $$;


ALTER PROCEDURE public.sp_add_bunos_salary() OWNER TO postgres;

--
-- Name: sp_allworkersfromsite(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_allworkersfromsite(_sitename text) RETURNS TABLE(id bigint, name text, phone text, salary integer, role text)
    LANGUAGE plpgsql
    AS $$
    declare
        id_site BIGINT:=0;
        begin
        SELECT "Sites".id into id_site FROM "Sites" WHERE "Sites".name=_siteName;

        return query
        SELECT "Workers".id,"Workers".name,"Workers".phone,"Workers".salary,R.name FROM "Workers"
            join "Roles" R on R.id = "Workers".role_id
        WHERE "Workers".site_id= id_site;
    end;
    $$;


ALTER FUNCTION public.sp_allworkersfromsite(_sitename text) OWNER TO postgres;

--
-- Name: sp_entersalaries(); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.sp_entersalaries()
    LANGUAGE plpgsql
    AS $$
    declare
        _role_id bigint:=0;
        begin
        for i in 1.. (SELECT max(id) from "Workers")
        loop
            select role_id into _role_id from "Workers" where id=i;
            if _role_id=1 then UPDATE "Workers" SET salary=20000 where id=i;
            else UPDATE "Workers" SET salary=Random()*5000::int+5000 where id=i;
            end if;
            end loop;
    end
    $$;


ALTER PROCEDURE public.sp_entersalaries() OWNER TO postgres;

--
-- Name: sp_getallworkers(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_getallworkers() RETURNS TABLE(id bigint, name text, phone text, role text)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
             SELECT "Workers".id,"Workers".name,"Workers".phone,R.name from "Workers"
                 join "Roles" R on R.id = "Workers".role_id;
    end;
    $$;


ALTER FUNCTION public.sp_getallworkers() OWNER TO postgres;

--
-- Name: sp_getmaxworkerssite(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_getmaxworkerssite() RETURNS TABLE(id bigint, name text, address text)
    LANGUAGE plpgsql
    AS $$
    declare
        max_workers INTEGER:=0;
        begin
        select count("Workers".id) workers_count into max_workers from "Workers" group by "Workers".site_id
        order by workers_count desc limit 1;

        return query
        SELECT tbl.id,tbl.name,tbl.address from (select "Sites".id,"Sites".name,"Sites".address,count(W.site_id) workers_count from "Sites"
        join "Workers" W on "Sites".id = W.site_id group by "Sites".id,"Sites".name,"Sites".address)tbl
        where tbl.workers_count=max_workers;
    end;
    $$;


ALTER FUNCTION public.sp_getmaxworkerssite() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Roles" (
    id bigint NOT NULL,
    name text NOT NULL
);


ALTER TABLE public."Roles" OWNER TO postgres;

--
-- Name: Roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Roles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Roles_id_seq" OWNER TO postgres;

--
-- Name: Roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Roles_id_seq" OWNED BY public."Roles".id;


--
-- Name: Sites; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Sites" (
    id bigint NOT NULL,
    name text NOT NULL,
    address text NOT NULL
);


ALTER TABLE public."Sites" OWNER TO postgres;

--
-- Name: Sites_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Sites_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Sites_id_seq" OWNER TO postgres;

--
-- Name: Sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Sites_id_seq" OWNED BY public."Sites".id;


--
-- Name: Workers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Workers" (
    id bigint NOT NULL,
    name text NOT NULL,
    phone text NOT NULL,
    salary integer,
    role_id bigint NOT NULL,
    site_id bigint NOT NULL
);


ALTER TABLE public."Workers" OWNER TO postgres;

--
-- Name: Workers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Workers_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Workers_id_seq" OWNER TO postgres;

--
-- Name: Workers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Workers_id_seq" OWNED BY public."Workers".id;


--
-- Name: Roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Roles" ALTER COLUMN id SET DEFAULT nextval('public."Roles_id_seq"'::regclass);


--
-- Name: Sites id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sites" ALTER COLUMN id SET DEFAULT nextval('public."Sites_id_seq"'::regclass);


--
-- Name: Workers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Workers" ALTER COLUMN id SET DEFAULT nextval('public."Workers_id_seq"'::regclass);


--
-- Data for Name: Roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Roles" (id, name) FROM stdin;
1	Manager
2	Builder
3	observer
\.


--
-- Data for Name: Sites; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Sites" (id, name, address) FROM stdin;
1	pool site	haneviim 7
2	museum site	rotschild 12
3	school site	rvivim 3
4	subway site	shlomo hamelech 1
\.


--
-- Data for Name: Workers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Workers" (id, name, phone, salary, role_id, site_id) FROM stdin;
2	Avi	050-1234567	20500	1	1
3	Beny	050-7654321	9991	2	1
4	Shlomi	050-5123987	7593	2	1
5	Adam	050-4320146	8708	3	1
6	Ezra	050-2013764	20500	1	2
7	Roey	050-3207941	10116	2	2
8	Nissim	050-3207648	9848	2	2
9	Dan	050-9531072	8965	3	2
10	Yoav	050-1320794	20500	1	3
11	Itay	050-1243765	8246	2	3
12	Ahmed	050-2134674	9840	2	3
13	Gad	050-2048967	7535	3	3
14	Liraz	050-2453701	20500	1	4
15	Danny	050-1048764	7580	2	4
16	Ariel	050-1012478	6440	2	4
17	Elli	050-1234608	6274	3	4
18	Ellimelch	050-1234651	6594	3	4
\.


--
-- Name: Roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Roles_id_seq"', 3, true);


--
-- Name: Sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Sites_id_seq"', 4, true);


--
-- Name: Workers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Workers_id_seq"', 18, true);


--
-- Name: Roles roles_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Roles"
    ADD CONSTRAINT roles_pk PRIMARY KEY (id);


--
-- Name: Sites sites_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sites"
    ADD CONSTRAINT sites_pk PRIMARY KEY (id);


--
-- Name: Workers workers_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Workers"
    ADD CONSTRAINT workers_pk PRIMARY KEY (id);


--
-- Name: Workers workers_roles__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Workers"
    ADD CONSTRAINT workers_roles__fk FOREIGN KEY (role_id) REFERENCES public."Roles"(id);


--
-- Name: Workers workers_sites_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Workers"
    ADD CONSTRAINT workers_sites_id_fk FOREIGN KEY (site_id) REFERENCES public."Sites"(id);


--
-- PostgreSQL database dump complete
--

