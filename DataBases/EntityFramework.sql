USE [TestEf]
GO
/****** Object:  Table [dbo].[Cities]    Script Date: 09/01/2021 22:39:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cities](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NOT NULL,
	[District_Id] [bigint] NOT NULL,
	[Mayor] [varchar](max) NOT NULL,
	[Population] [int] NOT NULL,
 CONSTRAINT [PK_Cities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Districts]    Script Date: 09/01/2021 22:39:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Districts](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[Population] [int] NULL,
 CONSTRAINT [PK_Districts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cities] ON 

INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (1, N'Tiberias', 1, N'Rami', 1500)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (2, N'Kiryat Shmona', 1, N'Nissim', 1000)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (3, N'Haifa', 1, N'Aliza', 2500)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (4, N'Eilat', 2, N'Avi', 3000)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (5, N'Ashdod', 2, N'Benny', 1600)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (6, N'Yeroham', 2, N'Dana', 500)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (7, N'Tel-Aviv', 3, N'Ron', 10000)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (8, N'Yafo', 3, N'Menahem', 1700)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (9, N'Petah-Tiqva', 3, N'Rami', 15000)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (10, N'Ramle', 4, N'Omer', 2600)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (11, N'Lod', 4, N'Adi', 2900)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (12, N'Jerusalem', 4, N'Roey', 1800)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (13, N'Herzelia', 5, N'Moshe', 1900)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (14, N'Hod-Hasharon', 5, N'Rami', 1500)
INSERT [dbo].[Cities] ([Id], [Name], [District_Id], [Mayor], [Population]) VALUES (15, N'Ramat-Hasharon', 5, N'Rami', 1500)
SET IDENTITY_INSERT [dbo].[Cities] OFF
GO
SET IDENTITY_INSERT [dbo].[Districts] ON 

INSERT [dbo].[Districts] ([Id], [Name], [Population]) VALUES (1, N'North', 5000)
INSERT [dbo].[Districts] ([Id], [Name], [Population]) VALUES (2, N'South', 5100)
INSERT [dbo].[Districts] ([Id], [Name], [Population]) VALUES (3, N'Center', 26700)
INSERT [dbo].[Districts] ([Id], [Name], [Population]) VALUES (4, N'Gosh Dan', 7300)
INSERT [dbo].[Districts] ([Id], [Name], [Population]) VALUES (5, N'Hasharon', 4900)
SET IDENTITY_INSERT [dbo].[Districts] OFF
GO
ALTER TABLE [dbo].[Cities]  WITH CHECK ADD  CONSTRAINT [FK_Cities_Districts] FOREIGN KEY([District_Id])
REFERENCES [dbo].[Districts] ([Id])
GO
ALTER TABLE [dbo].[Cities] CHECK CONSTRAINT [FK_Cities_Districts]
GO
