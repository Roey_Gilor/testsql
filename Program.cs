﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;

namespace Test
{
    class Program
    {       
         private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            my_logger.Info("******************** System startup");

            #region sqlite

            string connection = SqliteAppConfig.Instance.ConnectionString;
            float ver = SqliteAppConfig.Instance.Version;
            string app_name = SqliteAppConfig.Instance.AppName;

            Console.WriteLine($"Welcome to {app_name} ver {ver}");

            SqliteTest sqlite = new SqliteTest(connection);

            //Car car1 = new Car(0, "Usa", "Reno", 2020);
            //sqlite.UpdateCar(car1, 6);

            //Car car = new Car(0, "china", "Shevrolet", 2018);
            // sqlite.AddCar(car);

            //sqlite.DeleteCar(6);

            var listAllCars= sqlite.GetAllCars(connection);
            listAllCars.ForEach(item => Console.WriteLine(item.ToString()));

            Console.WriteLine();

            //Tests test = new Tests(0, 2, true, new DateTime(2020, 12, 31));
            //sqlite.AddTest(test);

            //Tests test1 = new Tests(0, 3, false, new DateTime(2020, 01, 07));
            //sqlite.UpdateTest(test1, 10);

            //sqlite.DeleteTest(10);

            var listAllTests = sqlite.GetAllTest(connection);
            listAllTests.ForEach(item => Console.WriteLine(item.ToString()));

            Console.WriteLine();

            var getAllByManufacturer = sqlite.GetAllCarsByManufacturer(connection, "USA");
            getAllByManufacturer.ForEach(item => Console.WriteLine(JsonConvert.SerializeObject(item)));

            var getAllInnerJoin = sqlite.GetAllCarsJoinTests(connection);
            getAllInnerJoin.ForEach(item => Console.WriteLine(JsonConvert.SerializeObject(item)));

            Console.WriteLine();
            #endregion

            #region SSMS
            string ssms_connection = MssqlAppConfig.Instance.ConnectionString;
            float ver_mssql = MssqlAppConfig.Instance.Version;
            string app_name_mssql = MssqlAppConfig.Instance.AppName;

            Console.WriteLine($"Welcome to {app_name_mssql} ver {ver_mssql}");

            SsmsTest ssmsTest = new SsmsTest(ssms_connection);

            //ssmsTest.AddStore(new Store(0, "Gadgets", 3, 1));

            //ssmsTest.UpdateStore(new Store(0, "T-Shirt", 2, 2), 9);

            ssmsTest.DeleteStore(9);

            var allStores = ssmsTest.GetAllStores();
            allStores.ForEach(item => Console.WriteLine(item.ToString()));

            Console.WriteLine();

            //var allStoresById = ssmsTest.GetStoreById(4);
            //allStoresById.ForEach(item => Console.WriteLine(item.ToString()));

            //ssmsTest.AddCategory(new Categories { Id = 0, Name = "Games" });

            //ssmsTest.UpdateCategory(new Categories { Id = 0, Name = "Pets shop" }, 5);

            ssmsTest.DeleteCategory(5);

            var allCategories = ssmsTest.GetAllCategories();
            allCategories.ForEach(item => Console.WriteLine(item.ToString()));

            Console.WriteLine();

            var getAllShoopsInCategoryAndFloor = ssmsTest.GetAllShoopsInCategoryAndFloor(2, "Books");
            getAllShoopsInCategoryAndFloor.ForEach(item => Console.WriteLine(JsonConvert.SerializeObject(item)));

            var allCategoriesById = ssmsTest.GetCategoryById(4);
            allCategoriesById.ForEach(item => Console.WriteLine(item.ToString()));

            var mostShopsInCategory = ssmsTest.GetMostShopsInCategory();
            mostShopsInCategory.ForEach(item => Console.WriteLine(item.ToString()));

            Console.WriteLine();
            #endregion

            #region Entity Framework
            EfTest ef = new EfTest();

            //ef.AddCity(new City { Name = "Rishon-Letzion", District_Id = 4, Mayor = "Shlomi", Population = 10000 });
            //ef.UpdateCity(new City { Name = "Beer-Sheva", District_Id = 2, Mayor = "Tzipora", Population = 100001 }, 16);
            //ef.DeleteCity(16);

            var citiesList= ef.GetAllCities();
            citiesList.ForEach(item => Console.WriteLine(JsonConvert.SerializeObject(item)));

            Console.WriteLine();

            Console.WriteLine(JsonConvert.SerializeObject(ef.GetCityById(1)));

            Console.WriteLine();

            //ef.AddDistrict(new District { Name = "West", Population = 125400 });
            //ef.UpdateDistrict(new District { Name = "Haarava", Population = 1400 }, 6);
            //ef.DeleteDistrict(6);

            var districtsList = ef.GetAllDistricts();
            districtsList.ForEach(item => Console.WriteLine(JsonConvert.SerializeObject(item)));

            Console.WriteLine();

            Console.WriteLine(JsonConvert.SerializeObject(ef.GetDistrictById(5)));

            Console.WriteLine();

            var citiesMoreThan = ef.CitiesMoreThan(3000);
            citiesMoreThan.ForEach(item => Console.WriteLine(JsonConvert.SerializeObject(item)));

            Console.WriteLine();

            var citiesByLinq = ef.GetAllCitiesByLinq();
            citiesByLinq.ForEach(item => Console.WriteLine(JsonConvert.SerializeObject(item)));

            //ef.AddDistrictsPopulation();

            #endregion

            #region Postgres part 1
            string postgres_connection = PostgreSqlAppConfig.Instance.ConnectionString;
            float ver_postgres = PostgreSqlAppConfig.Instance.Version;
            string app_name_postgres = PostgreSqlAppConfig.Instance.AppName;

            Console.WriteLine($"Welcome to {app_name_postgres} ver {ver_postgres}");
            PostgreSqlTest postgreSql = new PostgreSqlTest(postgres_connection);

            if (PostgreSqlTest.TestDbConnection(postgres_connection))
            {
                var allWorkersFromSite = postgreSql.SP_AllWorkersFromSite("pool site");
                foreach (var worker in allWorkersFromSite)
                {
                    foreach (var item in worker)
                    {
                        Console.WriteLine($"{item.Key} , {item.Value}");
                    }
                }
            }
            else
                Console.WriteLine("Cannot connect to db");
            #endregion

            #region IMDB
            string IMDB_connection = ImdbAppConfig.Instance.ConnectionString;
            float ver_IMDB = ImdbAppConfig.Instance.Version;
            string app_name_IMDB = ImdbAppConfig.Instance.AppName;

            Console.WriteLine($"Welcome to {app_name_IMDB} ver {ver_IMDB}");
            Console.WriteLine();
            PostgreSqlTest Imdb = new PostgreSqlTest(IMDB_connection);

            if (PostgreSqlTest.TestDbConnection(IMDB_connection))
            {
                //Imdb.AddMovie(new Movie { Id = 0, Name = "Walking on Water", Release_Date = new DateTime(2020, 1, 15), Genre_Id = 1 });

                //Imdb.UpdateMovie(new Movie { Id = 0, Name = "Walking on fire", Release_Date = new DateTime(2019, 4, 20), Genre_Id = 4 }, 9);

                //Imdb.DeleteMovie(9);

                var allMovies = Imdb.GetAllMovies();
                allMovies.ForEach(movie => Console.WriteLine(movie));

                Console.WriteLine();

                var allMoviesById = Imdb.GetAlMoviesById(1);
                allMoviesById.ForEach(movie => Console.WriteLine(movie));

                Console.WriteLine();

                //Imdb.AddGenre(new Genre { Id = 0, Name = "Autobiographic" });

                //Imdb.UpdateGenre(new Genre { Id = 0, Name = "Horror" }, 5);

                Imdb.DeleteGenre(5);

                var allGenres = Imdb.GetAllGenres();
                allGenres.ForEach(genre => Console.WriteLine(genre));

                Console.WriteLine();

                var allGenresById = Imdb.GetAllGenresById(1);
                allGenresById.ForEach(genre => Console.WriteLine(genre));

                Console.WriteLine();

                //Imdb.AddActor(new Actor { Id = 0, Name = "Wentworth Miller", Birth_Date = new DateTime(1976, 6, 3) });

                //Imdb.UpdateActor(new Actor { Id = 0, Name = "Shlomo Miller", Birth_Date = new DateTime(1990, 7, 3) }, 5);

                //Imdb.DeleteActor(5);

                var allActors = Imdb.GetAllActors();
                allActors.ForEach(actor => Console.WriteLine(actor));

                Console.WriteLine();

                var allActorsById = Imdb.GetActorById(1);
                allActorsById.ForEach(actor => Console.WriteLine(actor));

                Console.WriteLine();

                var allMoviesBefore = Imdb.GetAllMoviesWhenActorBefore1972();
                allMoviesBefore.ForEach(item => Console.WriteLine(JsonConvert.SerializeObject(item)));
            }
            else
                Console.WriteLine("Cant connect to db");
            #endregion

            my_logger.Info("******************** System shutdown");
        }
    }
}
