﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DAO
{
    public class Actor
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime Birth_Date { get; set; }
        public Actor()
        {
                
        }

        public Actor(int id, string name, DateTime birth_Date)
        {
            Id = id;
            Name = name;
            Birth_Date = birth_Date;
        }
        public override string ToString()
        {
            return $"{JsonConvert.SerializeObject(this)}";
        }
    }
}
