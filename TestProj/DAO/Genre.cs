﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DAO
{
    public class Genre
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public Genre()
        {

        }
        public Genre(long id, string name)
        {
            Id = id;
            Name = name;
        }
        public override string ToString()
        {
            return $"{JsonConvert.SerializeObject(this)}";
        }
    }
}
