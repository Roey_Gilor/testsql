﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DAO
{
    public class Store
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Floor { get; set; }
        public long Category_Id { get; set; }
        public Store()
        {

        }
        public Store(long id, string name, int floor, long category_Id)
        {
            Id = id;
            Name = name;
            Floor = floor;
            Category_Id = category_Id;
        }
        public override string ToString()
        {
            return $"{JsonConvert.SerializeObject(this)}";
        }
    }
}
