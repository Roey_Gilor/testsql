﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DAO
{
    public class Movie
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime Release_Date { get; set; }
        public long Genre_Id { get; set; }
        public Movie()
        {

        }

        public Movie(int id, string name, DateTime release_Date, int genre_Id)
        {
            Id = id;
            Name = name;
            Release_Date = release_Date;
            Genre_Id = genre_Id;
        }
        public override string ToString()
        {
            return $"{JsonConvert.SerializeObject(this)}";
        }
    }
}
