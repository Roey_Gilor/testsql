﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DAO
{
    public class Tests
    {
        public long Id { get; set; }
        public long Car_Id { get; set; }
        public bool Is_Passed { get; set; }
        public DateTime Date { get; set; }
        public Tests()
        {

        }

        public Tests(long id, long car_Id, bool is_Passed, DateTime date)
        {
            Id = id;
            Car_Id = car_Id;
            Is_Passed = is_Passed;
            Date = date;
        }
        public override string ToString()
        {
            return $"{JsonConvert.SerializeObject(this)}";
        }
    }
}
