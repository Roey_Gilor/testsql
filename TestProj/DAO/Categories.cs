﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DAO
{
    public class Categories
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public Categories()
        {

        }
        public Categories(long id, string name)
        {
            Id = id;
            Name = name;
        }
        public override string ToString()
        {
            return $"{JsonConvert.SerializeObject(this)}";
        }
    }
}
