﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using DAO;

namespace Test
{
    class SqliteTest
    {
        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region ctor + field
        private string m_conn_string;

        public SqliteTest(string conn_string)
        {
            m_conn_string = conn_string;
        }
        #endregion
        #region Get all cars method
        public List<Car> GetAllCars(string connection)
        {
            List<Car> cars = new List<Car>();
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connection))
                {
                    conn.Open();

                    using (SQLiteCommand select_query = new SQLiteCommand("SELECT * FROM Cars", conn))
                    {
                        using (SQLiteDataReader reader = select_query.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Car car = new Car
                                {
                                    Id = (long)reader["Id"],
                                    Manufacturer = reader["Manufacturer"].ToString(),
                                    Model = reader["Model"].ToString(),
                                    Year = (long)reader["Year"]
                                };
                                cars.Add(car);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to Get all cars. Error : {ex}");

            }
            return cars;
        }
        #endregion
        #region ExecuteNonQuery
        private int ExecuteNonQuery(string query)
        {
            int result = 0;
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    using (cmd.Connection = new SQLiteConnection(m_conn_string))
                    {
                        cmd.Connection.Open();

                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = query;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to make changes in data base. Error : {ex}");

            }
            return result;
        }
        #endregion
        #region Car methods
        public void AddCar (Car car)
        {
            ExecuteNonQuery($"INSERT INTO Cars (Manufacturer,Model,Year) VALUES ('{car.Manufacturer}','{car.Model}',{car.Year})");
        }
        public void UpdateCar(Car car, int id)
        {
            ExecuteNonQuery($"UPDATE Cars SET Manufacturer = '{car.Manufacturer}', Model = '{car.Model}', Year = '{car.Year}' WHERE Id = {id}");
        }
        public void DeleteCar(int id)
        {
            ExecuteNonQuery($"DELETE FROM Cars WHERE Id = {id}");
        }
        #endregion
        #region Get all tests method
        public List<Tests> GetAllTest(string connection)
        {
            List<Tests> tests = new List<Tests>();
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connection))
                {
                    conn.Open();

                    using (SQLiteCommand select_query = new SQLiteCommand("select * from Tests", conn))
                    {
                        using (SQLiteDataReader reader = select_query.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var i = reader["Date"];
                                Tests test = new Tests
                                {
                                    Id = (long)reader["Id"],
                                    Car_Id = (long)reader["Car_Id"],
                                    Is_Passed = (bool)reader["IsPassed"],
                                    Date = Convert.ToDateTime(reader["Date"])
                                };
                                tests.Add(test);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to Get all Tests. Error : {ex}");

            }
            return tests;
        }
        #endregion
        #region Tests methods
        public void AddTest(Tests test)
        {
            string day = test.Date.Day.ToString();
            string month = test.Date.Month.ToString();
            if (test.Date.Month < 10)
                month = $"0{test.Date.Month}";
            if (test.Date.Day < 10)
                day = $"0{test.Date.Day}";
            string date = $"{test.Date.Year}-{month}-{day}";
            ExecuteNonQuery($"INSERT INTO Tests (Car_Id,IsPassed,Date) VALUES ('{test.Car_Id}', '{test.Is_Passed}', '{date}')");
        }
        public void UpdateTest(Tests test, int id)
        {
            string day = test.Date.Day.ToString();
            string month = test.Date.Month.ToString();
            if (test.Date.Month < 10)
                month = $"0{test.Date.Month}";
            if (test.Date.Day < 10)
                day = $"0{test.Date.Day}";
            string date = $"{test.Date.Year}-{month}-{day}";
            ExecuteNonQuery($"UPDATE Tests SET Car_Id = {test.Car_Id}, IsPassed = '{test.Is_Passed}', Date = '{date}' WHERE Id = {id}");
        }
        public void DeleteTest(int id)
        {
            ExecuteNonQuery($"Delete FROM Tests WHERE Id = {id}");
        }
        #endregion
        #region get by manufacturer
        public List<Car> GetAllCarsByManufacturer(string connection, string manufacturer)
        {
            List<Car> cars = new List<Car>();
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connection))
                {
                    conn.Open();

                    using (SQLiteCommand select_query = new SQLiteCommand($"SELECT * FROM Cars WHERE Manufacturer = '{manufacturer}'", conn))
                    {
                        using (SQLiteDataReader reader = select_query.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Car car = new Car
                                {
                                    Id = (long)reader["Id"],
                                    Manufacturer = reader["Manufacturer"].ToString(),
                                    Model = reader["Model"].ToString(),
                                    Year = (long)reader["Year"]
                                };
                                cars.Add(car);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to Get all cars by manufacturer. Error : {ex}");

            }
            return cars;
        }
        #endregion
        #region GetAllCarsJoinTests
        public List<object> GetAllCarsJoinTests(string connection)
        {
            List<object> result = new List<object>();
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connection))
                {
                    conn.Open();
                    using (SQLiteCommand select_query =
                        new SQLiteCommand("SELECT Manufacturer,Model,Year,t.Id,t.IsPassed,t.Date from Cars JOIN Tests t" +
                        " where t.Car_Id = cars.Id;", conn))
                    {
                        using (SQLiteDataReader reader = select_query.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var one_row = new
                                {
                                    Id = (long)reader["Id"],
                                    Manufacturer = reader["Manufacturer"].ToString(),
                                    Model = reader["Model"].ToString(),
                                    Year = (long)reader["Year"]
                                };
                                result.Add(one_row);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to Get all cars joined to tests. Error : {ex}");

            }
            return result;
        }
        #endregion
    }
}
